let interval = 0;
let start = true;
let firstLog = true;

let logTableIndex = 0;
let timerCounter = 0;

window.onload = function () {
  const buttonStart = document.getElementById ('button-start');
  const displayTimeSecondary = document.getElementById ('secondary-display');

  buttonStart.innerHTML = 'Start';
  buttonStart.className = 'enable-start-btn';
  displayTimeSecondary.innerHTML = 'SPLIT TIME';

  resetMainDisplay ();
  setButtonStatus ('button-split', 'disable');
  setButtonStatus ('button-reset', 'disable');
};

function startBtnClicked () {
  clearInterval (interval);

  if (start) {
    interval = setInterval (startTimer, 1);

    setBtnToPause ();
    setButtonStatus ('button-split', 'enable');
  } else {
    //pause
    addToLogTable (++logTableIndex, 'Pause');

    setBtnToStart ();
    setButtonStatus ('button-split', 'disable');
    setButtonStatus ('button-reset', 'enable');
  }
}

function splitBtnClicked () {
  if (firstLog == true) {
    startLogTable ();
    firstLog = false;
  }
  addToLogTable (++logTableIndex, 'Split');
}

function resetBtnClicked () {
  const container = document.getElementById ('container');
  document.getElementById ('button-reset').className = 'enable-reset-btn';

  if (firstLog == true) {
    startLogTable ();
    firstLog = false;
  }
  clearInterval (interval);
  timerCounter = 0;

  setBtnToStart ();
  resetLogTable ();
  resetMainDisplay ();
  resetSecondaryDisplay ();
}

function startTimer () {
  ms = ++timerCounter % 1000;
  sec = pad (parseInt (timerCounter / 100 % 60, 10));
  min = pad (parseInt (timerCounter / (60 * 100), 10));
  hrs = pad (parseInt (timerCounter / (3600 * 100), 10));

  let timeLapsedArr = [hrs, min, sec, ms, timerCounter];

  setMainDisplay (timeLapsedArr);
  setSecondaryDisplay (timeLapsedArr);
}

function setBtnToStart () {
  const buttonStart = document.getElementById ('button-start');

  start = true;
  buttonStart.innerHTML = 'Start';
  buttonStart.className = 'enable-start-btn';
}

function setBtnToPause () {
  const buttonStart = document.getElementById ('button-start');

  start = false;
  buttonStart.innerHTML = 'Pause';
  buttonStart.className = 'enable-pause-btn';
}

function startLogTable () {
  const line = document.getElementById ('horizontal-line');
  elem = document.createElement ('hr');
  elem.setAttribute ('width', '100%');
  line.appendChild (elem);
}

function setButtonStatus (button, action) {
  if (action === 'disable') {
    document.getElementById (button).disabled = true;
    document.getElementById (button).className = 'disable-btn';
  } else {
    document.getElementById (button).disabled = false;
    if (button === 'button-split') {
      document.getElementById (button).className = 'enable-split-btn';
    } else {
      document.getElementById (button).className = 'enable-reset-btn';
    }
  }
}

function addToLogTable (index, eventType) {

  const container = document.getElementById ('container');

  let arr = [
    '#' + index,
    hrs + ':' + min + ':' + sec + '.' + timerCounter % 1000,
    eventType,
  ];

  for (let i = 0; i < arr.length; i++) {
    let cell = document.createElement ('div');
    cell.innerText = arr[i];
    container.appendChild (cell).className = 'grid-item';

    if (i == 1) {
      if (eventType === 'Pause') {
        container.appendChild (cell).classList.add ('magenta-text');
      } else {
        container.appendChild (cell).classList.add ('orange-text');
      }
    } else {
      container.appendChild (cell).classList.add ('gray-text');
    }
  }
}

function resetLogTable () {
  document.getElementById ('container').innerHTML = '';
  logTableIndex = 0;
}

function pad (val) {
  return val > 9 ? val : '0' + val;
}

function setMainDisplay (arr) {
  document.getElementById ('ms_lsb').innerHTML = arr[4] % 100;
  document.getElementById ('millisec').innerHTML = arr[3] % 10;
  document.getElementById ('seconds').innerHTML = arr[2];
  document.getElementById ('minutes').innerHTML = arr[1];
  document.getElementById ('hours').innerHTML = arr[0];
}

function setSecondaryDisplay (arr) {
  document.getElementById ('secondary-display').innerHTML =
    arr[0] + ':' + arr[1] + ':' + arr[2] + '.' + arr[4] % 1000;
}

function resetMainDisplay () {
  document.getElementById ('hours').innerHTML = '00';
  document.getElementById ('minutes').innerHTML = '00';
  document.getElementById ('seconds').innerHTML = '00';
  document.getElementById ('millisec').innerHTML = '0';
  document.getElementById ('ms_lsb').innerHTML = '00';
}

function resetSecondaryDisplay () {
  document.getElementById ('secondary-display').innerHTML =
    '00' + ':' + '00' + ':' + '00' + '.' + '000';
}
